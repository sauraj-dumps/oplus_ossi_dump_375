#!/bin/bash

cat system_ext/priv-app/SystemUI/oat/arm64/SystemUI.odex.* 2>/dev/null >> system_ext/priv-app/SystemUI/oat/arm64/SystemUI.odex
rm -f system_ext/priv-app/SystemUI/oat/arm64/SystemUI.odex.* 2>/dev/null
cat system_ext/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null >> system_ext/priv-app/SystemUI/SystemUI.apk
rm -f system_ext/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null
cat system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null >> system_ext/priv-app/Settings/Settings.apk
rm -f system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null
cat system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null >> system_ext/apex/com.android.vndk.v30.apex
rm -f system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null
cat vendor/lib64/libsdk_sr.so.* 2>/dev/null >> vendor/lib64/libsdk_sr.so
rm -f vendor/lib64/libsdk_sr.so.* 2>/dev/null
cat recovery.img.* 2>/dev/null >> recovery.img
rm -f recovery.img.* 2>/dev/null
